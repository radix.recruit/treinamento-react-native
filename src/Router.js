import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import ProfileScreen from './screens/ProfileScreen';
import FavoritesScreen from './screens/FavoritesScreen';
import DetailScreen from './screens/DetailScreen';
import Colors from './styles/Colors';
import {Image} from 'react-native';

const HomeStack = createStackNavigator();
const FavoritesStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStackScreen = () => {
  return(
    <HomeStack.Navigator>
      {/* <HomeStack.Screen name='Login' component={LoginScreen} options={{headerShown: false}}/> */}
      <HomeStack.Screen
      name='Home'
      component={HomeScreen}
      options={{
        title: 'RADIFLIX',
        headerStyle: {
          backgroundColor: Colors.black,
        },
        headerTintColor: Colors.white,
      }}
      />
      <HomeStack.Screen
      name='Detail'
      component={DetailScreen}
      options={{
        title: 'DETALHES',
        headerStyle: {
          backgroundColor: Colors.black,
        },
        headerTintColor: Colors.white,
      }}
      />
    </HomeStack.Navigator>
  )
}

const FavoritesStackScreen = () => {
  return (
    <FavoritesStack.Navigator>
      <FavoritesStack.Screen
      name='Favorites'
      component={FavoritesScreen}
      options={{
        title: 'FAVORITOS',
        headerStyle: {
          backgroundColor: Colors.black,
        },
        headerTintColor: Colors.white,
      }}
      />
    </FavoritesStack.Navigator>
  )
}

const ProfileStackScreen = () => {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name='Profile'
        component={ProfileScreen}
        options={{
            title: 'PERFIL',
            headerStyle: {
              backgroundColor: Colors.black,
            },
            headerTintColor: Colors.white,
          }}/>
    </ProfileStack.Navigator>
  )
}

const Router = () => {
    return(
        <Tab.Navigator
          tabBarOptions={{
            activeTintColor: Colors.green,
            inactiveTintColor: Colors.white,
            style: {
              backgroundColor: Colors.black
            }
          }}>
            <Tab.Screen
              name="Home"
              component={HomeStackScreen}
              options={{
                tabBarLabel: 'Início',
                tabBarIcon: ({focused, color}) => {
                  const image = focused
                    ? require('./assets/images/icon_home_focused.png')
                    : require('./assets/images/icon_home.png')
                  return ( <Image color={color} source={image} /> )
                }
              }}/>
            <Tab.Screen
              name="Favorites"
              component={FavoritesStackScreen}
              options={{
                tabBarLabel: 'Favoritos',
                tabBarIcon: ({focused, color}) => {
                  const image = focused
                    ? require('./assets/images/icon_fav_focused.png')
                    : require('./assets/images/icon_fav.png')
                  return ( <Image color={color} source={image} /> )
                }
              }}/>
            <Tab.Screen
              name="Profile"
              component={ProfileStackScreen}
              options={{
                tabBarLabel: 'Perfil',
                tabBarIcon: ({focused, color}) => {
                  const image = focused
                    ? require('./assets/images/icon_profile_focused.png')
                    : require('./assets/images/icon_profile.png')
                  return ( <Image color={color} source={image} /> )
                }
              }}/>
        </Tab.Navigator>
    )
}



export default Router;