import React from 'react';
import { StyleSheet, View, TextInput, Image } from 'react-native';
import Button from '../../components/Button/Button';
import Colors from '../../styles/Colors';

const LoginScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={require('../../assets/images/img_logo.png')}/>
      <TextInput
        style={styles.input}
        placeholder={'Email'}
        placeholderTextColor={Colors.white}
      />
      <TextInput
        style={styles.input}
        placeholder={'Password'}
        placeholderTextColor={Colors.white}
        secureTextEntry={true}
      />
      <Button text={'LOGIN'} action={() => navigation.navigate('Home')}/>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.purple,
    padding: 10,
    justifyContent: 'center',
  },
  logo: {
    alignSelf: 'center',
    height: 50,
    width: 150,
  },
  input: {
    color: Colors.white,
    borderBottomColor: Colors.white,
    borderBottomWidth: 1,
    marginBottom: 10,
  }
});

export default LoginScreen;
