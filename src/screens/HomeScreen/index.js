import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  SectionList,
} from 'react-native';
import MovieSectionList from '../../components/MovieSectionList';
import Colors from '../../styles/Colors';
import api from '../../services/api';

const HomeScreen = ({navigation}) => {
  const [movies, setMovies] = useState([]);

  const getMovies = async () => {
    try {
        let result = await api.get('/movies');
        setMovies(result.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getMovies();
  }, []);

  return (
    <View style={styles.screen}>
      <SectionList
        sections={movies}
        contentContainerStyle={{paddingHorizontal: 5}}
        renderItem={() => {return null}}
        renderSectionHeader={({section}) => (<MovieSectionList section={section} navigation={navigation}/>) }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: Colors.black,
    padding: 10
  },
  text: {
    fontSize: 14,
    color: Colors.white,
    marginTop: 10
  },
  card: {
    width: 100,
    height: 150,
    borderRadius: 10
  },
  imageView: {
    marginHorizontal: 5
  }
});

const mock = [
    {
      title: "Animation",
      data: [
          {
              key: 1,
              name: "SPIDER-MAN: INTO THE SPIDER-VERSE",
              releaseYear: 2018,
              genre: "Animation",
              synopsis: "Bitten by a radioactive spider in the subway, Brooklyn teenager Miles Morales suddenly develops mysterious powers that transform him into the one and only Spider-Man. When he meets Peter Parker, he soon realizes that there are many others who share his special, high-flying talents. Miles must now use his newfound skills to battle the evil Kingpin, a hulking madman who can open portals to other universes and pull different versions of Spider-Man into our world.",
              poster: "https://upload.wikimedia.org/wikipedia/pt/3/33/Spider-Man_Into_the_spider-verse_poster_c%C3%B3pia.png"
          },
          {
            key: 2,
            name: "THE LION KING",
            releaseYear: 1994,
            genre: "Animation",
            synopsis: "This Disney animated feature follows the adventures of the young lion Simba (Jonathan Taylor Thomas), the heir of his father, Mufasa (James Earl Jones). Simba's wicked uncle, Scar (Jeremy Irons), plots to usurp Mufasa's throne by luring father and son into a stampede of wildebeests. But Simba escapes, and only Mufasa is killed. Simba returns as an adult (Matthew Broderick) to take back his homeland from Scar with the help of his friends Timon (Nathan Lane) and Pumbaa (Ernie Sabella).",
            poster: "https://upload.wikimedia.org/wikipedia/en/3/3d/The_Lion_King_poster.jpg"
          },
          {
            key: 4,
            name: "TOY STORY",
            releaseYear: 1995,
            genre: "Animation",
            synopsis: "Woody (Tom Hanks), a good-hearted cowboy doll who belongs to a young boy named Andy (John Morris), sees his position as Andy's favorite toy jeopardized when his parents buy him a Buzz Lightyear (Tim Allen) action figure. Even worse, the arrogant Buzz thinks he's a real spaceman on a mission to return to his home planet. When Andy's family moves to a new house, Woody and Buzz must escape the clutches of maladjusted neighbor Sid Phillips (Erik von Detten) and reunite with their boy.",
            poster: "https://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg"
          },
          {
            key: 5,
            name: "WALL-E",
            releaseYear: 2008,
            genre: "Animation",
            synopsis: "WALL-E, short for Waste Allocation Load Lifter Earth-class, is the last robot left on Earth. He spends his days tidying up the planet, one piece of garbage at a time. But during 700 years, WALL-E has developed a personality, and he's more than a little lonely. Then he spots EVE (Elissa Knight), a sleek and shapely probe sent back to Earth on a scanning mission. Smitten WALL-E embarks on his greatest adventure yet when he follows EVE across the galaxy.",
            poster: "https://upload.wikimedia.org/wikipedia/pt/thumb/3/30/WALL-E.jpg/250px-WALL-E.jpg"
          }
      ]
    },
    {
      title: "Documentary",
      data: [
          {
            key: 1,
            name: "APOLLO 11",
            releaseYear: 2019,
            genre: "Documentary",
            synopsis: "Apollo 11 is a 2019 American documentary film edited, produced and directed by Todd Douglas Miller. It focuses on the 1969 Apollo 11 mission, the first spaceflight from which men walked on the Moon. The film consists solely of archival footage, including 70 mm film previously unreleased to the public, and does not feature narration, interviews or modern recreations. The Saturn V rocket, Apollo crew consisting of Buzz Aldrin, Neil Armstrong, and Michael Collins, and Apollo program Earth-based mission operations engineers are prominently featured in the film.",
            poster: "https://upload.wikimedia.org/wikipedia/en/2/29/Apollo_11_%282019_film%29.png"
          },
          {
            key: 2,
            name: "THE LAST WALTZ",
            releaseYear: 1978,
            genre: "Documentary",
            synopsis: "Seventeen years after joining forces as the backing band for rockabilly cult hero Ronnie Hawkins, Canadian roots rockers The Band call it quits with a lavish farewell show at San Francisco's Winterland Ballroom on Nov. 25, 1976. Filmed by Martin Scorsese, this documentary features standout performances by rock legends such as Bob Dylan, Van Morrison, Eric Clapton, Joni Mitchell and Muddy Waters, as well as interviews tracing the group's history and discussing road life.",
            poster: "https://upload.wikimedia.org/wikipedia/en/thumb/5/5e/LastWaltzMoviePoster.jpg/220px-LastWaltzMoviePoster.jpg"
          },
          {
            key: 3,
            name: "WON'T YOU BE MY NEIGHBOR?",
            releaseYear: 2018,
            genre: "Documentary",
            synopsis: "Filmmaker Morgan Neville examines the life and legacy of Fred Rogers, the beloved host of the popular children's TV show Mister Rogers' Neighborhood.",
            poster: "https://upload.wikimedia.org/wikipedia/en/7/7d/Won%27t_You_Be_My_Neighbor%3F.png"
          }
      ]
    },
    {
      title: "Science-Fiction",
      data: [
          {
            key: 1,
            name: "INTERSTELLAR",
            releaseYear: 2014,
            genre: "Science-Fiction",
            synopsis: "nterstellar is a 2014 epic science fiction film co-written, directed and produced by Christopher Nolan. It stars Matthew McConaughey, Anne Hathaway, Jessica Chastain, Bill Irwin, Ellen Burstyn, and Michael Caine. Set in a dystopian future where humanity is struggling to survive, the film follows a group of astronauts who travel through a wormhole near Saturn in search of a new home for humanity.",
            poster: "https://upload.wikimedia.org/wikipedia/en/b/bc/Interstellar_film_poster.jpg"
          },
          {
            key: 2,
            name: "ANNIHILATION",
            releaseYear: 2018,
            genre: "Science-Fiction",
            synopsis: "Annihilation is a 2018 British-American science fiction horror film written and directed by Alex Garland, based on the 2014 novel of the same name by Jeff VanderMeer. It stars Natalie Portman, Jennifer Jason Leigh, Gina Rodriguez, Tessa Thompson, Tuva Novotny, and Óscar Isaac. The story follows a group of explorers who enter The Shimmer, a mysterious quarantined zone of mutating plants and animals caused by an alien presence.",
            poster: "https://upload.wikimedia.org/wikipedia/en/f/f6/Annihilation_%28film%29.png"
          },
          {
            key: 3,
            name: "2001: A SPACE ODYSSEY",
            releaseYear: 1968,
            genre: "Science-Fiction",
            synopsis: "2001: A Space Odyssey is a 1968 epic science fiction film produced and directed by Stanley Kubrick. The screenplay was written by Kubrick and Arthur C. Clarke, and was inspired by Clarke's 1951 short story The Sentinel and other short stories by Clarke. A novel released after the film's premiere was in part written concurrently with the screenplay. The film follows a voyage to Jupiter with the sentient computer HAL after the discovery of an alien monolith, and deals with themes of existentialism, human evolution, technology, artificial intelligence, and the possibility of extraterrestrial life.",
            poster: "https://upload.wikimedia.org/wikipedia/en/1/11/2001_A_Space_Odyssey_%281968%29.png"
          }
      ]
    }
];

export default HomeScreen;