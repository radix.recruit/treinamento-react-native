import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Colors from '../../styles/Colors';

const DetailScreen = ({props}) => {
  return (
    <View style={styles.container}>
    {console.log("PROPS NOVA", props)}
      <View style={styles.movie}>
        <Image style={styles.poster} source={{uri: "https://upload.wikimedia.org/wikipedia/pt/3/33/Spider-Man_Into_the_spider-verse_poster_c%C3%B3pia.png"}}/>
        <View style={styles.info}>
          <Text style={styles.title}>Titulo</Text>
          <Text style={styles.defaultText}>Ano</Text>
        </View>
      </View>
      <Text style={styles.description}>Descrição</Text>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
    padding: 10,
  },
  movie: {
    flexDirection: 'row'
  },
  info: {
    paddingLeft: 10,
  },
  poster: {
    width: 150,
    height: 200,
    borderRadius: 10
  },
  title: {
    fontSize: 18,
    color: Colors.green
  },
  defaultText: {
    fontSize: 14,
    color: Colors.white
  },
  description: {
    marginTop: 10,
    color: Colors.white
  },
});
export default DetailScreen;