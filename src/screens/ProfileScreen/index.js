import React from 'react';
import { StyleSheet, Image, View, Text} from 'react-native';
import Button from '../../components/Button/Button';
import Colors from '../../styles/Colors';

const ProfileScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Image source={require('../../assets/images/img_avatar.png')} style={styles.avatar}/>
        <View>
          <Text style={styles.userName}>JOSÉ DA SILVA</Text>
          <Text style={styles.address}>Rio de Janeiro/RJ</Text>
       </View>
      </View>
       <Button text={'Configurações'}/>
       <Button text={'Sair'}/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
    padding: 10,
  },
  text: {
    color: Colors.white
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 15
  },
  userName: {
    fontSize: 18,
    color: Colors.green
  },
  address: {
    fontSize: 14,
    color: Colors.white
  },
  subContainer: {
    flexDirection: 'row',
    marginBottom: 30,
    marginTop: 20
  }

});

export default ProfileScreen;