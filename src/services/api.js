import axios from 'axios';

const baseUrl = "https://private-2c6c5-movieapi132.apiary-mock.com";

const api = axios.create({
  baseURL: baseUrl,
  headers: {
    "Content-Type": "application/json"
  },
  timeout: 10000
});

export default api;