import 'react-native-gesture-handler';
import React from 'react';
import { SafeAreaView, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Router from './Router';

//https://private-2c6c5-movieapi132.apiary-mock.com/users
const App = () => {
  return (
      <NavigationContainer>
        <SafeAreaView style={{flex: 1}}>
          <Router/>
        </SafeAreaView>
      </NavigationContainer>
  );
};

export default App;
