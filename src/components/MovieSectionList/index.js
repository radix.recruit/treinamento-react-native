import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import Colors from '../../styles/Colors';
import Card from '../Card';

const MovieSectionList = ({section, navigation}) => {
  return (
    <View>
      <Text style={styles.text}>{section.title}</Text>
      <FlatList
        horizontal
        data={section.data}
        renderItem={({item}) => <Card props={item} navigation={navigation}/> }
      />
    </View>
)}

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    color: Colors.white,
    marginTop: 10
  },
  card: {
    width: 100,
    height: 150,
    borderRadius: 10
  },
});

export default MovieSectionList;