import React from 'react';
import {Image, View, StyleSheet, TouchableHighlight} from 'react-native';

const Card = ({props, navigation}) => (
  <TouchableHighlight style={styles.imageView} onPress={() => navigation.navigate('Detail', {props})}>
    <Image
      style={styles.card}
      source={{uri: props.poster}}
    />
  </TouchableHighlight>
);

const styles = StyleSheet.create({
  card: {
    width: 100,
    height: 150,
    borderRadius: 7.5,
  },
  imageView: {
    marginHorizontal: 5
  }
});

export default Card;
