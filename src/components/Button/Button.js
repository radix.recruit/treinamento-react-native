import React from 'react';
import {View, Text, TouchableHighlight, StyleSheet} from 'react-native';
import Colors from '../../styles/Colors';

const Button = (props) => {
  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={props.action}>
        <Text style={styles.text}>{props.text}</Text>
      </TouchableHighlight>
    </View>
)}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.green,
    justifyContent: "center",
    padding: 10,
    borderRadius: 10,
    marginTop: 10
  },
  text: {
      color: Colors.purple,
      fontWeight: 'bold',
      textAlign: 'center'
  }
})

export default Button;