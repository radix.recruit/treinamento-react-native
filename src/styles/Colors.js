export default {
    purple: '#4B1C5B',
    green: '#BAC224',
    white: '#FFFFFF',
    black: '#000000',
}